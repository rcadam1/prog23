// Programa que, a partir d'un sencer introduït des de teclat, mostra tots els sencers, des d'1, menors que ell.

public class e5{

    public static void main(String args[]){

        int num;

        System.out.print("Introduce un valor: ");
        num = Integer.parseInt(System.console().readLine());
        System.out.println("---Numeros inferiores a " + num + "---");

        for(int i = 1; i < num; i++)
            System.out.print(i + "\t");    
        
        System.out.println();

    }
}
