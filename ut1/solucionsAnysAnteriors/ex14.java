
public class ex14
{
    public static void main(String args[])
    {
        double a, b, c;
        System.out.println("Introduzca el valor de A");
        a=Double.parseDouble(System.console().readLine());
        
        System.out.println("Introduzca el valor de B");
        b=Double.parseDouble(System.console().readLine());

        c=a; //c coge el valor de a
        a=b; //a coge el valor de b
        b=c; //B coge el valor de C

        System.out.println("Valor de A:" + a + " Valor de B:" + b);
    }
}
