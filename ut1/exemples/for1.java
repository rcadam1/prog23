// Primer exemple d'un bucle for: mostrarà els múltiples de 5 positius i inferiors a 100

public class for1
{
    public static void main(String args[])
    {
        // int cont;
        for( int cont = 0 ; cont < 100; cont = cont + 5)    // cont += 5
            System.out.println(cont);
    }
}

/*  Una solució alternativa al for anterior podria ser:

        for ( int cont = 0 ; cont < 100; cont++ )
            if (cont % 5 == 0)
                System.out.println(cont);

        Estem obligant al programa a fer moltes més passades, i cada vegada comprovar si cont és múltiple de 5

*/
