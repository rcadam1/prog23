/*
3. Realitza un programa en Java que faça el següent (solució preferentment modular, tot en una mateixa classe que incloga main i la resta de funcions):

 - genere 100 nombres enters aleatoris, entre 1 i 100, i els carregue en un array de 100 enters
 - mostre l'array per pantalla
 - demane a l'usuari un valor (entre 1 i 100 també) i indique si, o no, es troba en l'array (cerca lineal en vector desordenat)
 - ordene l'array amb el mètode de la bambolla
 - torne a mostrar l'array
 - demane un altre valor, entre 1 i 100, a l'usuari
 - realitze una altra cerca (ara dicotòmica) indicant, igualment, si es troba, o no, en l'array
 - amb el mateix valor de cerca, finalment realitzarà una cerca lineal indicant quantes vegades ha aparegut el valor en l'array
*/
import java.util.Scanner;

public class e3
{
	public static void main(String[] args) 
	{
		int N=100;
		int i=0;
		int busqueda=0;
		int aux;
		boolean ordenado=false;
		Scanner ent=new Scanner(System.in);
		int centena[]= new int[N];
		int limite=(centena.length-2);
		for(i=0;i<N;i++)
			centena[i]=(int)(100*(Math.random()) + 1);
		for(i=0;i<N;i++)
			System.out.println("Posicion "+(i+1)+":"+centena[i]);
		System.out.println("Introduce un valor a buscar");
		busqueda=ent.nextInt();
		if((buscaValorLineal(centena, busqueda))==true)
			System.out.println("El valor SI esta en el Array");
		else
			System.out.println("El valor NO esta en el Array");
		// ordenació
		while((limite>=0) &&(ordenado==false))
		{
			ordenado=true;
			for(i=0;i<=limite;i++)
			{
				if(centena[i+1]<centena[i])
					{
						aux=centena[i];
						centena[i]=centena[i+1];
						centena[i+1]=aux;
						ordenado=false;
					}
			}
			limite--;
		}
		for(i=0;i<N;i++)
			System.out.println("Posicion "+(i+1)+":"+centena[i]);
		System.out.println("Introduce un valor a buscar");
		busqueda=ent.nextInt();
		if((buscaValorDico(centena,busqueda)))
		{
			System.out.println("El valor SI esta en el Array");
			System.out.println("El valor se ha encontrado "+ buscaValorLinealContado(centena, busqueda) +" veces.");
		}		
		else
			System.out.println("El valor NO esta en el Array");
		
		
	}
	public static boolean buscaValorLineal(int posiciones[], int num)
	{
		//boolean resultado=false;
		for(int i=0;i<(posiciones.length);i++)
			if(posiciones[i]==num)
				return true;
		return false;
	}
	public static boolean buscaValorDico(int num[], int valor)
	{
		int izq=0, der=num.length-1, centro, cont=0;;
		centro=(izq+der)/2;
		while((izq<=der) && (num[centro]!=valor))
		{
			//si entra aqui es por que el valor central no es el que buscamos
				if(num[centro]>valor)
					der=centro-1;
				else
					izq=centro+1;
				centro=(izq+der)/2;
		}
		if(num[centro]==valor)
			return true;
		return false;
	}
	public static int buscaValorLinealContado(int posiciones[], int num)
	{
		boolean resultado=false;
		int cont=0;
		for(int i=0;i<(posiciones.length);i++)
			if(posiciones[i]==num)
			{
				resultado=true;
				cont++;
			}
		//System.out.println("El valor se ha encontrado "+cont+" veces.");
		return cont;
	}
}
