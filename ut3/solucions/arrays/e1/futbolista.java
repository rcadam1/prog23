public class futbolista  
{
	private String name;
	private int goals;
	
	//constructores
	public futbolista()
	{
		name="Vacío";
		goals=0;
	}
	
	public futbolista(String name, int goals)
	{
		this.name=name;
		this.goals=goals;
	}
	
	public futbolista(futbolista f)
	{
		name=f.name;
		goals=f.goals;
	}
	
	//setters
	public void setName(String n)
	{
		name=n;
	}
	
	public void setGoals(int g)
	{
		goals=g;
	}
	
	//getters
	public String getName()
	{
		return name;
	}
	
	public int getGoals()
	{
		return goals;
	}
	
	public void printPlayer()
	{
		System.out.println("El futbolista "+name+" ha marcado "+goals+" goles");
	}
	
}
