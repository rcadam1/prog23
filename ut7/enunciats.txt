1. Crear una aplicació a Java que cree una nova instància de Base de dades anomenada prova2 i afija una taula anomenada venedors.

2. Crear un objecte ResultSet sobre la taula clients que permeta desplaçament lliure, però que no veja els canvis en les dades i permeta realitzar modificacions sobre les dades.

3. Es vol realitzar un programa en Java que, de manera atòmica, realitze un increment d'un 10% en el sou de tots els seus venedors i faça un increment d'un 5% en el preu de tots els seus articles de la base de dades empresa.

4. Actualitzar el preu de 3 files incrementant-los un 5%, 6% i un 7%, respectivament.

5. Crear una aplicació a Java que connecte a la base de dades empresa, cree un ResultSet de tipus "scrollable" i mostre el conjunt de factures en ordre invers on aparega l'identificador de factura, la sèrie, el número, la data, el nom del client i el venedor. Després mostrarem, per aquest ordre, les següents files: última, primera, tercera i segona.

6. Realitzar una aplicació gràfica (amb Swing) que amb un botó (etiquetatge "Següent") permeta mostrar els valors de tots els camps per a cada article de la taula corresponent. Amb cada clic ha de mostrar els valors corresponents al següent article.

7. Modifica l'anterior exercici per a afegir un botó "Anterior", de manera que l'aplicació permeta retrocedir o avançar, un a un, sobre tots els articles de la taula.

8. Crea un programa visual que permeta afegir discos a la taula d'igual nom.

Un botó permetrà la connexió a la base de dades, i l'altre donar d'alta cada disc segons el valor dels seus camps.

9. Crea un programa visual que permeta incrementar el preu d'un disc, segons un determinat valor, indicant el seu identificador.

10. Sobre la taula "discos" de la BBDD "musica", i utilitzant PreparedStatement, actualitza el preu dels discos 5 i 6 a 10 euros.
