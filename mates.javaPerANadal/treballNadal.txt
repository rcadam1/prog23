PRIMERA PART
-------------------

Se't demana realitzar un programa que permeta a xiquets d'educació primària exercitar el càlcul aritmètic (sumes, restes, productes i divisions). El programa aleatòriament generarà dos operands (números entre 1 i 10) i un operador (+,-,*,/), mostrant l'operació corresponent. Per exemple podrà mostrar:

	7 / 4 = ?
	
L'usuari o xiquet haurà de respondre introduint el valor correcte corresponent al resultat de l'operació i el programa acabarà mostrant un missatge que indique la correcció, o no, de la resposta. En el cas de la divisió, com en l'exemple anterior, la resposta esperada és el quocient sense tindre en compte el residu. És a dir, per a a l'exemple anterior serà 1, no 1.75.

Per a la resolució de l'exercici se't demana:

	- defineix una classe Operand amb un atribut numèric sencer. Aquest valor es generarà aleatòriament en el constructor de la classe (entre 1 i 10).
	
	- defineix una classe Operador amb un atribut de tipus caràcter. El constructor de la classe generarà aleatòriament un valor entre 4 possibles (+,-,*./) per a aquest atribut.
	
	- afig a totes dues classes un mètode getValor() que retorne el valor de l'atribut.
	
	- afig a totes dues classes un mètode anomenat "toString" amb el següent prototip:
		public String toString()
		
	. Aquest mètode retornarà el valor de l'atribut convertint-lo prèviament a String.
	
	- crea un programa en el qual s'instancien 2 operands i un operador. Seguint amb l'exemple de dalt, els operands serien 7 i 4, i l'operador seria el de la divisió.

SEGONA PART
-------------

Realitza una segona part de l'exercici en la qual el programa, en lloc de demanar la solució a una única operació, ho faça per a 10 operacions aritmètiques generades d'igual forma. El programa acabarà mostrant una nota (entre 0 i 10), corresponent al nombre de preguntes respostes correctament.

A més, el programa emmagatzemarà en un (o més) objecte/s Vector (o array ordinari, qui vulga tornar a practicar arrays) cadascuna de les operacions contestades erròniament per a tornar-les a preguntar, tantes vegades faça falta, fins que siguen contestades correctament totes. Cada vegada que una pregunta siga contestada correctament serà eliminada dels arrays o Vector corresponents. Per a iterar, en el cas d'utilitzar la classe Vector, sobre cadascun dels vectors amb els operands i operadors pendents de resoldre, s'aconsella utilitzar el mètode iterator() de la pròpia classe Vector.

