//Realiza un programa que permita gestionar un fichero secuencial y binario
//con los empleados de una empresa (num de empleado, nombre y salario)-
//Menú con opciones: alta, llistar todos los empleados y consultar empleado a partir de numero.
import java.util.*;
import java.io.*;

public class e5b{
	static int num;
	static int sal;
	static String nom;
	static Scanner ent= new Scanner(System.in);
	static File f= new File("empleados.dat");

	public static void main(String[] args) {
		int opc;

		do{
			System.out.println("Menú:");
			System.out.println("1. Alta de nuevo empleado.");
			System.out.println("2. Mostrar todos los empleados registrados");
			System.out.println("3. Consultar empleado a partir de su número de empleado");
			System.out.println("0. Salir");
			opc=ent.nextInt();

			switch(opc){
			case 1: alta();break;
			case 2: mostrar(); break;
			case 3: consultar(); break;
			case 0: System.exit(0); break;
			default: System.out.println("Opción inválida");
			}

		}while(true);
	}

	public static void alta(){

		try(
				FileOutputStream fos= new FileOutputStream(f, true);
				DataOutputStream dos= new DataOutputStream(fos);
		){
			
			System.out.println("Introduce el número de empleado para darlo de alta");
			num=ent.nextInt();
			ent.nextLine();
			System.out.println("Introduce el nombre de empleado para darlo de alta");
			nom=ent.nextLine();
			System.out.println("Introduce el salario de empleado para darlo de alta");
			sal=ent.nextInt();
			ent.nextLine();

				dos.writeUTF(nom);
				dos.writeInt(num);
				dos.writeInt(sal);
				System.out.println(nom+"\t"+ num + "\t" + sal);

			System.out.println("Dada de alta exitosa");

		}catch(IOException e){
			System.err.println(e.getMessage());
		}
	}
	
	public static void mostrar(){
		try(
			FileInputStream fis=new FileInputStream(f);
			DataInputStream dis= new DataInputStream(fis);
		){
			String linea=dis.readLine();
			while(linea!=null){
				System.out.println(linea);
				linea=dis.readLine(); //sale que está depricado por el API?, pero en mi casa estuve practicando y no me dio ese error...
				// HAS DE LLEGIR PER SEPARAT CADA VALOR: OBSERVA LES LÍNIES 52 A54 --> ARA TOCA FER READUTF(), READINT() I READINT(): CADA VALOR PER SEPARAT
				// NO ÉS UN FITXER DE TEXT EN EL QUE TINGUES UN EMPLEAT EN CADA LÍNIA
			}

		}catch(IOException e){
			System.err.println(e.getMessage());
		}
	}

	public static void consultar(){
		/*try(){

		}catch(IOException e){
			System.err.println(e.getMessage());
		}*/
	}
}
