//6.Paquete Resolubles, 2 clases y una interfaz.

import java.util.*;

interface Resoluble{
	public double valor();
}

class Potencia implements Resoluble{
	protected double base;
	protected double exponente;

	public Potencia(){
		base=4.2;
		exponente=2.2;
	}

	public Potencia(double b, double e){
		base=b;
		exponente=e;
	}
	public Potencia(Potencia p){
		base=p.base;
		exponente=p.exponente;
	}

	public void setBase(double b){
		base=b;
	}
	public void setExponente(double e){
		exponente=e;
	}
	public double getBase(){
		return base;
	}
	public double getExponente(){
		return exponente;
	}
	public String toString(){
		return base + "^" + exponente;
	}
	public void potencia(double n){
		Scanner ent= new Scanner(System.in);	// NO L'HAS DE DEMANAR: JA EL TENS COM A PARÀMETRE
		System.out.println("Introduce el valor de n para actualizar el exponente");
		n=ent.nextDouble();
		exponente=n*exponente; //exponente actualizado
	}
	public double valor(){
		return Math.pow(base,exponente);
	}
}
class Potencia10 extends Potencia{

	public Potencia10(double e){
		// SUPER() HA D'ESTAR AL PRINCIPI
		base=10.0;
		exponente=e; //me daba error al poner super(e)
	}
	public Potencia10(Potencia10 p10){
		super(p10);
	}
	@Override
	public double valor(){
		double redondeo= Math.round(Math.pow(base,exponente)); //redondear
		return redondeo;	// AMB 2 DECIMALS
	}
}
public class e6{
	public static void main(String[] args) {
		
		Potencia p= new Potencia(4.2 , 2.2);
		Potencia10 p10= new Potencia10(2.5); //solo pide el exponente
		System.out.println(p.valor()); 
		System.out.println(p.toString());	// NO FA FALTA .TOSTRING
		System.out.println(p10.valor());
		System.out.println(p10.toString());
	}
}
