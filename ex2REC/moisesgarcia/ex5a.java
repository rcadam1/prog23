//exercici que accepte un text amb parametres de execució i lé transforma
public class ex5a{
	public static void main(String[] args) {
		//per  a pasarlo per execucio
		if(args.length==1)
			for(String texto:args)
			{
				
				System.out.println(texto);
				//convertim el texto tot a minuscules
				String textom=texto.toLowerCase();
				System.out.println(textom);
				//convertim el texto tot a mayuscules
				String textoM=texto.toUpperCase();
				System.out.println(textoM);
				//quitem els espai en blanc sobrant
				String textot=texto.trim();
				System.out.println(textot);
				System.out.println("te una mida de "textot.length());// COMPTAR PARAULES: STRINGTOKENIZEER
		
			}
			else
				// ADVERTEIX DE L'ÚS DE LES COMETES DOBLES " " O NO FUNCIONARÀ CORRECTAMENT
				System.out.println("este programa se utiliza de la siguente manera: java ex5a texto a introducir");
	}
}
