import java.io.*;
import java.util.*;

public class e5b
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		do
		{
			System.out.println("\n1.alta de empleado\n2.listar a todos los empleados\n3.consultar un empleado a partir de su codigo\n0.Salir de la aplicacion");

			int n =sc.nextInt();
			switch(n)
			{
				case 1: alta();break;
				case 2: listar();break;
				case 3: System.out.println("introduce el codigo del empleado");
				n=sc.nextInt();
				consultar(n);break;
				default: System.exit(0);
			}
		}while(true);	
	}
	public static void alta()
	{
		int numero;
		String nombre;
		int salario;
		File f = new File("ficheros/empleadosbinario.txt");	// NO .TXT, ÉS BINARI
		try(FileOutputStream fos = new FileOutputStream(f,true);
			DataOutputStream dos = new DataOutputStream(fos);
			// NO NECESSITES ELS SEGÜENTS
			FileOutputStream fos2 = new FileOutputStream("empleados.dat",true);
			DataOutputStream dos2 = new DataOutputStream(fos2);)
		{
			Scanner n = new Scanner(System.in);
			System.out.println("Introduce el numero del empleado");
			numero=n.nextInt();
			n.nextLine();
			System.out.println("introduce el nombre del empleado");
			nombre=n.nextLine();
			System.out.println("introduce el salario del empleado");
			salario=n.nextInt();

			dos.writeInt(numero);
			dos.writeUTF(nombre);
			dos.writeInt(salario);
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
	public static void listar()
	{
		int numero;
		String nombre;
		int salario;		
		File f = new File("ficheros/empleadosbinario.txt");
		try(FileInputStream fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			// NO NECESSITES ELS SEGÜENTS
			FileOutputStream fos2 = new FileOutputStream("empleados.dat",true);
			DataOutputStream dos2 = new DataOutputStream(fos2);)
		{
			while(true)
			{
				numero=dis.readInt();
				nombre=dis.readUTF();
				salario=dis.readInt();

				System.out.println("\nnombre: " + nombre +  "\nsalario: " + salario + "\nnumero: " + numero +"\n");
			}
		}
		catch(EOFException e)
		{
			//System.err.println(e.getMessage());
		}
		catch(IOException e1)
		{
			System.err.println(e1.getMessage());
		}
	}
	public static void consultar(int numero1)
	{
		int numero;
		String nombre;
		int salario;
		File f = new File("ficheros/empleadosbinario.txt");
		try(FileInputStream fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			// NO NECESSITES ELS SEGÜENTS
			FileOutputStream fos2 = new FileOutputStream("empleados.dat",true);
			DataOutputStream dos2 = new DataOutputStream(fos2);)
		{
			numero=dis.readInt();
			nombre=dis.readUTF();
			salario=dis.readInt();

			while(numero != numero1)
			{
				numero=dis.readInt();
				nombre=dis.readUTF();
				salario=dis.readInt();
			}
			if(numero == numero1)
			{
				System.out.println("\nNombre: " + nombre + "\nsalario: " + salario);
			}
		}
		catch(IOException e1)
		{
			System.err.println();
		}
	}
}
