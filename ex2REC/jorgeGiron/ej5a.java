import java.io.*;
import java.util.Scanner;

public class ej5a {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int contador = 0;

		System.out.println("Introduzca el texto a modificar:");
		String texto = sc.nextLine();	// S'HAVIA DE PASAR COM A PARÀMETRE A MAIN

		//Esto hara que lo pase tanto a mayusculas como a minusculas, cada uno en uno distinto para no alterar el texto principal.
		String mayus = texto.toUpperCase();
		System.out.println(mayus);
		String minus = texto.toLowerCase();
		System.out.println(minus);

		//Contador de carcteres.
			// CARACTERS O PARAULES? UTILITZA STRINGTOKENIZER
		for(int i=0; i < texto.length(); i++)
			contador++;
		
		

		System.out.println("Han sido " + contador +" palabras");

	}
}
