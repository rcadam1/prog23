

public class potencia 
{
	private double base;
	private double exponente;


	public potencia()
	{
		this.base = 0;
		this.exponente =0;
	}

	public potencia(double base, double exponente)
	{
		this.base = base;
		this.exponente = exponente;
	}

	public potencia(potenia otrapotencia)
	{
		this.base = otrapotencia.base;
		this.exponente = otrapotencia.exponente;
	}

	public void setbase(double base)
	{
		this.base = base;
	}

		public double getbase()
	{
		return base;
	}

	public void setexponente(double exponente)
	{
		this.exponente = exponente;
	}

	public double getexponente()
	{
		return exponente;
	}
}