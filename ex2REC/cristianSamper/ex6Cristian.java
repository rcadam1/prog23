import java.io.*;

interface Resoluble
{
	public double valor();
}

class Potencia implements Resoluble
{
	double base, exponente;	// PROTECTED
	public Potencia()
	{
		base=2.8;
		exponente=3;
	}
	public Potencia(double b, double e)
	{
		base=b;
		exponente=e;		
	}
	public Potencia(Potencia p)
	{
		base=p.getBase();	// POTS FER p.base I p.exponente
		exponente=p.getExponente();
	}
	public void potencia(double n)
	{
		exponente=(exponente*n);
	}
	public void setBase(double b)
	{
		base=b;
	}
	public void setExponente(double e)
	{
		exponente=e;
	}
	public double getBase()
	{
		return base;
	}
	public double getExponente()
	{
		return exponente;
	}
	public double valor()
	{
		return Math.pow(base,exponente);
	}
	// FALTA TOSTRING()
}

class Potencia10 extends Potencia
{
	double base=10;	// AL CONSTRUCTOR
	public Potencia10(double e)
	{
		Potencia p=new Potencia(base,e);	// NO: HA DE SER POTENCIA10,  ASIGNA 10 SEMPRE A LA BASE I A L'EXPONENT EL VALOR DEL PARÀMETRE
		// HAVIA CRIDAR A SUPER
	}
	public int getBase10()
		{
			return 10;
		}
	public double getExponente10()
	{
		return exponente;
	}
	// @OVERRRIDE I NOMÉS 2 DECIMALS
	public double valor()
	{
		return Math.pow(base,exponente);
	}

}

public class ex6Cristian
{
	public static void main(String[] args)
	{
		Potencia p1=new Potencia();
		Potencia p2=new Potencia(5.4,2);
		Potencia p3=new Potencia(p2);
		Potencia10 p4=new Potencia10(4.2);
		System.out.println("Potencia 1: "+p1.getBase()+" elevado a "+p1.getExponente()+" es "+p1.valor());
		System.out.println("Potencia 2: "+p2.getBase()+" elevado a "+p2.getExponente()+" es "+p2.valor());
		System.out.println("Potencia 3: "+p3.getBase()+" elevado a "+p3.getExponente()+" es "+p3.valor());
		System.out.println("Potencia 4: "+p4.getBase10()+" elevado a "+p4.getExponente10()+" es "+p4.valor());
		p3.setBase(3);
		System.out.println("Potencia 3: "+p3.getBase()+" elevado a "+p3.getExponente()+" es "+p3.valor());
		p2.potencia(2);
		System.out.println("Potencia 2: "+p2.getBase()+" elevado a "+p2.getExponente()+" es "+p2.valor());

	}
}
