import java.util.*;

public class ex5aCristian
{
	public static void main(String[] args)
	{
		if(args.length>0)
		{
			int cont=0;
			String texto=args[0],textofinal;
			StringTokenizer tokentexto= new StringTokenizer(texto);
			textofinal=tokentexto.nextToken()+" ";
			cont++;
			// NO NECESSITES RECOMPONDRE EL TEXT DE PARTIDA, NI COMPTAR TOKER (EXISTEIX COUNTTOKENS())
			// NO EL MOSTRES EN MINÚSCULES I MAJÚSCULES
			while(tokentexto.hasMoreElements())
			{
				textofinal=(textofinal+tokentexto.nextToken()+" ");
				cont++;
			}
			textofinal=textofinal.trim();
			System.out.println("["+textofinal+"]");
			System.out.println("Han habido "+cont+" palabras");
		}
		else
			System.out.println("Este programa usa la siguiente forma: java ex5aCristian [cadena de texto]");
	}
}
