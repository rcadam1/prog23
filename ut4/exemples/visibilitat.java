// Exemple de sobreescriptura: el tipus d'objecte, i no la referència, determinen a quin mètode "hola()" estem cridant

class A {
/*	void hola() {
		System.out.println("Estic en A");
	}*/
}
class B extends A {
	void hola() {
		System.out.println("Estic en B");
	}
}
class C extends A {
	void hola() {
		System.out.println("Estic en C");
	}
}

class visibilitat {
	public static void main (String args []) {
		A a = new A();
		B b = new B();
		C c = new C();
		A r; // Obtenció d'una referència a un objecte A
		r = a;
		((C)r).hola();
		// Exemple d'upcasting (referència a la classe base, objecte de la classe derivada)
		r = b;
		((B)r).hola();
		// Exemple d'upcasting (referència a la classe base, objecte de la classe derivada)
		r = c;
		((C)r).hola();
	}
}

// Amb una referència a la classe base (línia 24) no tinc "visibilitat" del contingut de les classes derivades

// Si el mètode està sobreeescrit es crida al métode de la classe derivada

// Si el mètode no existeix en la classe base no puc directament cridar-lo si la referència és a la classe base, però puc fer un "casting" de la referència
