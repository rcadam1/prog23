// Classe BASE ABSTRACTA: NO PODRÀ INSTANCIAR-SE

package figures;

public abstract class Figura
{
	protected String color;
	
	public Figura() { color = "negre";}
	
	public Figura(String c) { color = c; }
	
	public void setColor(String c) { color = c; }
	
	public String getColor() { return color; }
	
	public String toString() { return "El color de la figura és " + color; }
	
	public abstract double area();
	
	public abstract double perimetre();
}
