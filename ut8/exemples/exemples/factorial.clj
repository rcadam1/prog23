; Solució recursiva, amb loop-recur, del factorial

(defn factorial [n]
	(loop [numeroActual n total 1]
		(if (= numeroActual 1)
			total
		(recur (dec numeroActual) (* total numeroActual))
		)
	)
)

(println (factorial 5))
