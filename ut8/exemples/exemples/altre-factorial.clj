(defn altre-factorial
"Altra versió del factorial, amb 2 paràmetres"
[n total]	; requereix 2 paràmetres
(if (<= n 1)
	total
	(recur (dec n) (* total n))
)
)

(println (altre-factorial 5 1))
