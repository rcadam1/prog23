; 1. Funció "primeraLletra" que retorne el primer caracter d'un text

(defn primeraLletra
    "Retorna la primera lletra"
    [x]
    (.charAt x 0)
)


(println "Introduix un text")
(def text (read-line))
(println "La primera lletra és " (primeraLletra text))
