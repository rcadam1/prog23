; 5. Funció que accepte 2 textos com a paràmetres i comprove si el text segón es l'invers del primer. Per exemple, 'abcd' i 'dcba'. Pots utilitzar 'reverse'.

(defn reversibles
    "comprova si 2 textos són el mateix però en la seqüència inversa"
    [s1 s2]
    (if (= (reverse s1) (reverse (reverse s2)))
        true
        false
    )
)

(println "Introduix 2 línies de text")
(def primera (read-line))
(def segona (read-line))
(if (reversibles primera segona)
    (println "Són iguals però a la inversa")
    (println "Són diferents")
)
(println primera)
(println segona)
