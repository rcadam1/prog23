; 3. Funció "interval" que reba 2 paràmetres i mostre una llista amb tots els valors entre ells
; 4.  Igual que l'anterior però retornant els valors individuals, no com a llista (usant doseq).

(defn interval
    "retorna tots els valors entre els 2 paràmetres"
    [x y]
    (if (< x y)
        (doseq [n (range x y)] (println n) )
        (doseq [n (range y x)] (println n) )
    )
)

(println "Introduix 2 valors")
(def x (read)) 
(def y (read))
(print (interval x y))
