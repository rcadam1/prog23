; 3. Funció "interval" que reba 2 paràmetres i mostre una llista amb tots els valors entre ells

(defn interval
    "retorna tots els valors entre els 2 paràmetres"
    [x y]
    (if (< x y)
        (range x y)
        (range y x)
    )
)

(println "Introduix 2 valors")
(def x (read)) 
(def y (read))
(print (interval x y))
(println (interval 1 10))
