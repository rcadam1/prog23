; 2. Funció "length" que retorne la longitud en caracters d'un text

(defn length
    "Retorna la longitud d'un text"
    [x]
    (.length x)
)

(println "Introduix un text")
(def text (read-line))
(println "La longitud del text és " (length text))
