/*
 Programa que demane línies de text, fins a acabar amb una cadena buida, i les escriga a fitxer utilitzant la classe FileWriter */
 import java.io.*;
 import java.util.Scanner;

 public class exFileWriter
 {
    public static void main(String[] args) {
        Scanner ent = new Scanner(System.in);
        try
        {
        	// el segon paràmetre a true permet mantindre el contingut anterior del fitxer
            FileWriter fw = new FileWriter("linies.txt",true);
            System.out.println("Introduix línia (INTRO per a acabar):");
            String linia = ent.nextLine();
            while (!linia.isEmpty())   // entrarà quan linia NO estiga buida
            {
                fw.write(linia + "\n");
                linia = ent.nextLine();
            }
            fw.close();
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
    }
 }
