public class reloj
{
    private int horas;
    private int minutos;
    private int segundos;

    //Constructores
    //Por defecto
    public reloj()
    {
        horas=12;
        //minutos=0;
        //segundos=0;
    }
    //general
    public reloj(int horas, int minutos, int segundos)
    {
        this.horas = horas;	// atribut = paràmetre
        this.minutos=minutos;
        this.segundos=segundos;
    }

    //setters
    public void setHoras(int horas)
    {
        this.horas=horas;	// atribut = param
    }
    public void setMinutos(int m)
    {
        minutos=m;
    }
    public void setSegundos(int s)
    {
        segundos=s;
    }

    //getters
    public int getHoras()
    {
        return horas;
    }
    public int getMinutos()
    {
        return minutos;
    }
    public int getSegundos()
    {
        return segundos;
    }

    //Funciones de la clase

    //obtener los segundos que han pasado desde las 12 hasta la hora que se pasa a la funcion
    public void mostrarReloj()
    {
        if(horas<10)
            System.out.print("0");
        System.out.print(horas+":");
        if(minutos<10)
            System.out.print("0");
        System.out.print(minutos+":");
        if(segundos<10)
            System.out.print("0");
        System.out.println(segundos);
    }
    public int medianoche()
    {
        return ((horas*3600)+(minutos*60)+segundos);
    }

    public void sumaSegundos(int segs)
    {
        //FORMA CON FORMULAS
        segs=((horas*3600)+(minutos*60)+(segundos)+segs);
        
        horas=(segs/3600)%24;
        minutos=((segs-(horas*3600))/60)%60;
        segundos=((segs-(horas*3600))%60);
        
        if(horas>=24)
        	horas-=24;
        

        //forma con bucle
        /*
        segundos+=segs;
        while((segundos>59)||(minutos>59))
        {
            if(segundos>59)
            {
                minutos++;
                segundos-=60;
            }
            if(minutos>59)
            {
                horas++;
                minutos-=60;
            }
            if(horas>=24)
                horas=0;
        }
        */
        
    }
}
