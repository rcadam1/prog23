// Classe per a crear objectes Rectangle

public class usaRectangle
{
	public static void main(String[] args) 
	{
		// crearé 2 rectangles
/*		Rectangle r1 = new Rectangle();	// crida al constructor de Rectangle
		Rectangle r2 = new Rectangle();  // crida al constructor de Rectangle
		r1.mostraRectangle();
		r2.mostraRectangle();
		// ara canvie els amples i alts
		// r1.ample = 4;	// NO PUC MODIFICAR DIRECTAMENT L'ATRIBUT PER SER PRIVATE
		r1.setAmple(4);
		r1.setAlt(3);
		r1.mostraRectangle();
		r2.setAmple(5);
		r2.setAlt(2);
		r2.mostraRectangle(); */
		
		Rectangle r1 = new Rectangle(4,3);	// crida al constructor general
		Rectangle r2 = new Rectangle(5,2);
		Rectangle r3 = new Rectangle();	// crida al constructor per defecte
		// Cree un nou rectangle còpia de r2
		Rectangle r4 = new Rectangle(r2);
		// Mostrar perímetre del primer i àrea del segón
		r1.mostraRectangle();
		r2.mostraRectangle();
		r3.mostraRectangle();
		r4.mostraRectangle();
		System.out.println("Perímetre de r1: " + r1.perimetre());
		System.out.println("Àrea de r2: " + r2.area());
		
		System.out.println(r1);	// System.out.println(r1.toString()); TOSTRING HERETAT DE LA CLASSE OBJECT
		System.out.println(r2);
		System.out.println(r3);
		System.out.println(r4);
		
		
		
		System.exit(0);

	}
}
